
<!DOCTYPE html PUBLIC c"-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<%@ page import="java.util.List" %>
<%@ page import="il.hit.natali.amit.model.Groceries" %>
<title>Search</title>
</head>
<body>
<%@page language="java" contentType="text/html; charset=windows-1255" pageEncoding="windows-1255"%><%@include file="header.jsp" %>

<form method="get" data-ajax="false" >
 <select name="Groceries" id="Groceries" multiple="multiple" data-native-menu="false">
 <% 
 List<Groceries> list = (List<Groceries>) request.getAttribute("groceries");
 for(Groceries grocery : list)
{
%>       
          <option value="<% out.print(grocery.getId());%>"><% out.print(grocery.getName());%></option>
<%}%>
</select>
<input type="submit" value="ok" /> 
</form>

</body>
</html>