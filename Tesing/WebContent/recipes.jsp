<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<%@ page import="java.util.List" %>
<%@ page import="java.util.Set" %>
<%@ page import="il.hit.natali.amit.model.Recipes" %>
<%@ page import="il.hit.natali.amit.model.Groceries" %>
<title>Recipes</title>
</head>
<body>
<%@include file="header.jsp" %>
<%List<Recipes> list = (List<Recipes>) request.getAttribute("recipes");
if(list.size() == 0)
{%>
<h3 align="center">There are no recipes that match the search </h2>
<%}%>

<ul data-role="listview" data-inset="true">
<%
for(Recipes recipe : list)
{
%>

	<li><a href="<% out.print("recipes?id="+recipe.getId());%>" rel="external">
	<% out.print(recipe.getName());%>
	<p>
    <% 
	Set<Groceries> groceriesSet = recipe.getGroceries();
	for(Groceries grocery : groceriesSet)
	{
		out.print(grocery.getName()+", ");
	}
	%> 
	</p>
	</a> 
	</li>
<%
}
%>
</ul>
</body>
</html>