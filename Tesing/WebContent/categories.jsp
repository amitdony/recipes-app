<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<%@ page import="java.util.List" %>
<%@ page import="il.hit.natali.amit.model.Category" %>
<title>Categories</title>
</head>
<body>
<%@include file="header.jsp" %>

<ul data-role="listview" data-inset="true" >
 
<%
List<Category> list = (List<Category>) request.getAttribute("categories");
for(Category category : list)
{
%>
<li><a ><% out.print(category.getName());%></a></li>
<%
}
%>
</ul>

</body>
</html>