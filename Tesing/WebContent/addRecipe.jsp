<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<%@ page import="java.util.List" %>
<%@ page import="il.hit.natali.amit.model.Groceries" %>
<title>Manager</title>
</head>
<body>
<%@include file="manager.jsp" %>

<form method="get" name="recipesForm" action="addRecipe" data-ajax="false">
<p>Name: <input type="text" name=name> </p> 
<p>PreparationTime:
<label for="time"></label>
 <input type="range" name="time" id="points" value="0" min="0" max="100" data-popup-enabled="true" name=PreparationTime>
  </p> 
<p>Difficulty: 
 <select name="Difficulty" id="Difficulty" data-native-menu="false">
          <option value="easy">easy</option>
          <option value="medium">medium</option>
          <option value="hard">hard</option>
 </select>
</p> 
<p>Preparation:<textarea name="Preparation" id="info" name=Preparation></textarea> </p> 
<p>Groceries:</p> 
 <select name="Groceries" id="Groceries" multiple="multiple" data-native-menu="false">
 <% 
 List<Groceries> list = (List<Groceries>) request.getAttribute("groceries");
 for(Groceries grocery : list)
{
%>       
          <option value="<% out.print(grocery.getId());%>"><% out.print(grocery.getName());%></option>
<%}%>
</select>
<input type="submit" value="add" />
</form>



</body>
</html>