<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Grocery</title>
<%@ page import="java.util.Set" %>
<%@ page import="il.hit.natali.amit.model.Recipes" %>
<%@ page import="il.hit.natali.amit.model.Groceries" %>
<jsp:useBean class="il.hit.natali.amit.model.Groceries"  id="grocery" scope="request" />
</head>
<body>
<%@include file="header.jsp" %>
<p style="font-size: 30px " align="center"> <% out.println(grocery.getName());%> </p>


<ul data-role="listview" data-inset="true">
<%
Set<Recipes> set = (Set<Recipes>) grocery.getRecipes();
for(Recipes recipe : set)
{
%>
	<li><a href="<% out.print("recipes?id="+recipe.getId());%> " rel="external"><% out.print(recipe.getName());%>
	<p>
    <% 
	Set<Groceries> recipeSet = recipe.getGroceries();
	for(Groceries groceryItem : recipeSet)
	{
		out.print(groceryItem.getName()+", ");
	}
	%> 
	</p>
	</a> 
	</li>
<%
}
%>
</ul>
</body>
</html>