<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>An error has occurred!</title>
</head>
<body>
<% String details = (String)request.getAttribute("Details");  %>
<h1 align="center">Oops</h1>
<h2 align="center">An error has occurred! </h2>
<h3 align="center"> <%out.print(details); %> </h3>
<div align="center">
<img src=../images/error.jpg>
</div>
</body>
</html>