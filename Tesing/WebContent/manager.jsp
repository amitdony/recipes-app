<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="../themes/test.min.css" />
<link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
<link rel="stylesheet" href="../themes/jquery.mobile.structure-1.4.2.min.css" />
<script src="../themes/jquery-1.10.2.min.js"></script>
<script src="../themes/jquery.mobile-1.4.2.min.js"></script>
</head>
<body>
  <div data-role="main" class="ui-content" align="center">
    <h1 align="/center">Manager - Recipes App </h1>
     <a href="../Controller/index" class="ui-btn ui-btn-inline"  rel="external" >Logout</a>
    <a href="../Controller/editGroceries" class="ui-btn ui-btn-inline"  rel="external" >Edit Groceries</a>
    <a href="../Controller/editRecipes" class="ui-btn ui-btn-inline"  rel="external">Edit Recipes</a>
    <a href="../Controller/editCategories" class="ui-btn ui-btn-inline"  rel="external">Edit Categories</a>
  </div>
</body>
</html>