<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="../themes/test.min.css" />
<link rel="stylesheet" href="../themes/jquery.mobile.icons.min.css" />
<link rel="stylesheet" href="../themes/jquery.mobile.structure-1.4.2.min.css" />
<script src="../themes/jquery-1.10.2.min.js"></script>
<script src="../themes/jquery.mobile-1.4.2.min.js"></script>
</head>
<body >

  <div data-role="main" class="ui-content" align="center">
    <h1 align="/center">Recipes App </h1>
    <a href="../Controller/login" class="ui-btn ui-btn-inline  ui-icon-gear ui-btn-icon-right "  rel="external">Login</a>
    <a href="../Controller/groceries" class="ui-btn ui-btn-inline"  rel="external" >Groceries</a>
    <a href="../Controller/recipes" class="ui-btn ui-btn-inline"  rel="external">Recipes</a>
    <a href="../Controller/search" class="ui-btn ui-icon-search ui-btn-icon-right ui-btn-inline"  rel="external">Search</a>
    <a href="../Controller/about" class="ui-btn ui-btn-inline ui-icon-info ui-btn-icon-right"  rel="external">About</a>
  </div>
</body>
</html>