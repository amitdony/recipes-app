<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<%@ page import="java.util.List" %>
<%@ page import="java.util.Set" %>
<%@ page import="il.hit.natali.amit.model.Recipes" %>
<%@ page import="il.hit.natali.amit.model.Groceries" %>
<title>Insert title here</title>
</head>
<body>
<%@include file="manager.jsp" %>

<div align="center"> <a href="../Controller/addRecipe"   class="ui-btn ui-btn-inline"  rel="external">Add New</a> </div>

<ul data-role="listview" data-inset="true">
<%
List<Recipes> list = (List<Recipes>) request.getAttribute("recipes");
for(Recipes recipe : list)
{
%>

	<li><a  href=<% out.print("../Controller/editRecipes?id="+recipe.getId()+"&method=delete");%> rel="external" class="ui-btn ui-icon-delete ui-btn-icon-right">
	<% out.print(recipe.getName());%>
	<p>
    <% 
	Set<Groceries> groceriesSet = recipe.getGroceries();
	for(Groceries grocery : groceriesSet)
	{
		out.print(grocery.getName()+", ");
	}
	%> 
	</p>
	</a> 
	</li>
<%
}
%>
</ul>
</body>
</html>