<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<%@ page import="java.util.List" %>
<%@ page import="il.hit.natali.amit.model.Category" %>
<title>addGrocerie</title>
</head>
<body>
<%@include file="manager.jsp" %>

<form method="get" action="addGrocerie" data-ajax="false">
<p>Name: <input type="text" name=name> </p>
 
 <select name="Category" id="Category" data-native-menu="false">
 <% 
 List<Category> list = (List<Category>) request.getAttribute("Categories");
 for(Category category : list)
{
%>       
          <option value="<% out.print(category.getID());%>"><% out.print(category.getName());%></option>
<%}%>
</select>
<input type="submit" value="add" />
</form>

</body>
</html>