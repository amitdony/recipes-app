<%@ page language="java" contentType="text/html; charset=windows-1255" pageEncoding="windows-1255"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Insert title here</title>
</head>
<body>
<%@include file="header.jsp" %>
<div data-role="collapsible" data-collapsed="false" align="center">
<p align="center">This app allows users to find recipes by groceries that they have. </p> 
<div>
<u>How does it work?</u> 
<br>You want to cook something special, but you have only a few basic groceries?
<br> Enter your ingredients and click search,
  <br>within a few seconds you'll get a list of recipes that you can prepare.
</div>	
<p align="center"><br><br>Created by Natali and Amit (c)</p> 
</body>
</html>