<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Groceries</title>
<%@ page import="java.util.List" %>
<%@ page import="il.hit.natali.amit.model.Category" %>
<%@ page import="il.hit.natali.amit.model.Groceries" %>

</head>
<body>

<%@include file="header.jsp" %>

<ul data-role="listview" data-inset="true" >
<%
List<Groceries> list = (List<Groceries>) request.getAttribute("groceries");
for(Groceries grocery : list)
{
%>
<li><a href="<% out.print("groceries?id="+grocery.getId());%>" rel="external"> <% out.print(grocery.getName());%> 
<p><% out.print(grocery.getCategory().getName());%></p></a>
</li>
<%
}
%>
</ul>
</body>
</html>