<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<%@ page import="java.util.List" %>
<%@ page import="il.hit.natali.amit.model.Category" %>
<%@ page import="il.hit.natali.amit.model.Groceries" %>
<title>Insert title here</title>
</head>
<body>
<%@include file="manager.jsp" %>

 <div align="center"> <a href="../Controller/addGrocerie"   class="ui-btn ui-btn-inline"  rel="external">Add New</a> </div>

<ul data-role="listview" data-inset="true" >
<%
List<Groceries> list = (List<Groceries>) request.getAttribute("groceries");
for(Groceries grocery : list)
{
%>
<li><a  href=<% out.print("../Controller/editGroceries?id="+grocery.getId()+"&method=delete");%> rel="external"  class="ui-btn ui-icon-delete ui-btn-icon-right"> <% out.print(grocery.getName());%>
<p><% out.print(grocery.getCategory().getName());%></p></a>
</li>
<%
}
%>
</ul>
</body>
</html>