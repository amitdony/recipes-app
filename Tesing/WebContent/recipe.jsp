<%@ page language="java" contentType="text/html; charset=windows-1255"
    pageEncoding="windows-1255"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Recipe</title>
<%@ page import="java.util.List" %>
<%@ page import="il.hit.natali.amit.model.Groceries" %>
</head>
<body style="height: 283px; ">
<jsp:useBean class="il.hit.natali.amit.model.Recipes"  id="recipe" scope="request" />
<%@include file="header.jsp" %>

  <div data-role="collapsible" data-collapsed="false">		
			<p>Name: <% out.println(recipe.getName());%></p>
			<p>PreparationTime: <% out.println(recipe.getPreparationTime());%></p>
			<p>Difficulty: <% out.println(recipe.getDifficulty());%></p>
			<p>Preparation:<br>
                        <% out.println(recipe.getPreparation());%></p>
			<p>Groceries:<br>
		<% 
		for(Groceries grocery : recipe.getGroceries())
		{
			out.println(grocery.getName()+",");
		}
		%></p>
  </div>
</body>
</html>