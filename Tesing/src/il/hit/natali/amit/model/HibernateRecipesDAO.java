package il.hit.natali.amit.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * This class implements the IRecipesDAO interface and using Hibernate access object platform 
 * 
 * @author Amit and Natali
 * 
 * @param factory A sessionFactory field
 * @param instance A singletone field 
 */
public class HibernateRecipesDAO implements IRecipesDAO {
	private SessionFactory factory;

	public static HibernateRecipesDAO instance;

	/**
	 * Gets the singletone instance
	 * 
	 * @return An HibernateRecipesDAO instance 
	 */
	public static HibernateRecipesDAO getInstance() {
		if (instance == null) {
			instance = new HibernateRecipesDAO();
		}
		return instance;
	}
	
	/**
	 * private HibernateRecipesDAO constructor
	 */
	private HibernateRecipesDAO() {
		factory = new AnnotationConfiguration().configure()
				.buildSessionFactory();
	}

	/**
	 * Adds a new Recipe to the database
	 */
	@Override
	public boolean addRecipe(Recipes ob) throws RecipesPlatformException {
		Session session = null;
		try {
			session = factory.openSession();
			session.beginTransaction();
			session.saveOrUpdate(ob);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			if (session != null) {
				session.getTransaction().rollback();
			}
			throw new RecipesPlatformException("Failed adding a new recipe",
					e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	/**
	 * Gets a Recipe from the database based on the id of that recipe 
	 */
	@Override
	public Recipes getRecipe(int id) throws RecipesPlatformException {
		Session session = null;

		try {
			session = factory.openSession();
			Recipes toRet=(Recipes) session.get(Recipes.class, id);
			//Hibernate.initialize(toRet);
			//Hibernate.initialize(toRet.getGroceries());
			return toRet;

		} catch (HibernateException e) {

			throw new RecipesPlatformException("Failed getting a recipe", e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/**
	 * Gets all the Recipes from the database
	 */
	@Override
	public List<Recipes> getAllRecipes() throws RecipesPlatformException {
		Session session = null;
		
		try
		{
			session = factory.openSession();
			HashSet hs = new HashSet();
			hs.addAll(session.createCriteria(Recipes.class).list());
			ArrayList<Recipes> temp = new ArrayList<>(hs);
			return temp;
			 
		}
		catch(HibernateException e)
		{

			throw new RecipesPlatformException("Failed getting all recipes", e);			
		}
		finally
		{
			if(session != null)
			{
			session.close();
			}
		}
	}

	/**
	 * Deletes a Recipe from the database
	 */
	@Override
	public boolean deleteRecipe(int id) throws RecipesPlatformException 
	{
		Session session = null;
		boolean isRecipeDeleted = false;
		try
		{
			session = factory.openSession();
			Recipes recipe;
			session.beginTransaction();
			if ((recipe = (Recipes) session.get(Recipes.class, id)) != null) 
			{
	                session.delete(recipe);   
	                session.getTransaction().commit();
	                isRecipeDeleted = true; 
			}
			return isRecipeDeleted;
		}
		catch(HibernateException e)
		{
			throw new RecipesPlatformException("Failed deleting a recipe", e);
		}
		finally 
		{
			if (session != null) 
			{
				session.close();
			}
		}
	}
}
