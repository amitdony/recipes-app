package il.hit.natali.amit.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * This class implements the IGroceriesDAO interface and using Hibernate access object platform 
 * 
 * @author Amit and Natali
 * 
 * @param factory A sessionFactory field
 * @param instance A singletone field 
 */
public class HibernateGroceriesDAO implements IGroceriesDAO {
	private SessionFactory factory;

	public static HibernateGroceriesDAO instance;

	/**
	 * Gets the singletone instance
	 * 
	 * @return An HibernateGroceriesDAO instance 
	 */
	public static HibernateGroceriesDAO getInstance() {
		if (instance == null) {
			instance = new HibernateGroceriesDAO();
		}
		return instance;
	}

	/**
	 * private HibernateGroceriesDAO constructor
	 */
	private HibernateGroceriesDAO() {
		factory = new AnnotationConfiguration().configure()
				.buildSessionFactory();
	}

	/**
	 * Adds a new Grocery to the database
	 */
	public boolean addGrocery(Groceries ob) throws RecipesPlatformException {
		Session session = null;
		try {
			session = factory.openSession();
			session.beginTransaction();
			session.save(ob);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			if (session != null) {
				session.getTransaction().rollback();
			}
			throw new RecipesPlatformException("Failed adding a new grocery", e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/**
	 * Gets a Grocery from the database based on the id of that grocery 
	 */
	public Groceries getGrocery(int id) throws RecipesPlatformException {
		Session session = null;

		try {
			session = factory.openSession();
			return (Groceries) session.get(Groceries.class, id);

		} catch (HibernateException e) {

			throw new RecipesPlatformException("Failed getting a grocery", e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/**
	 * Gets all the Groceries from the database
	 */
	@Override
	public List<Groceries> getAllGroceries() throws RecipesPlatformException {
		Session session = null;
		
		try
		{
			session = factory.openSession();
			HashSet hs = new HashSet();
			hs.addAll(session.createCriteria(Groceries.class).list());
			ArrayList<Groceries> temp = new ArrayList<>(hs);	
			return temp;
			 
		}
		catch(HibernateException e)
		{

			throw new RecipesPlatformException("Failed getting all groceries", e);			
		}
		finally
		{
			if(session != null)
			{
			session.close();
			}
		}

	}
	
	/**
	 * Deletes a Grocery from the database
	 */
	@Override
	public boolean deleteGrocery(int id) throws RecipesPlatformException 
	{
		Session session = null;
		boolean isGroceryDeleted = false;
		try
		{
			session = factory.openSession();
			Groceries grocery;
			session.beginTransaction();
			if ((grocery = (Groceries) session.get(Groceries.class, id)) != null) 
			{
	                session.delete(grocery);
	                session.getTransaction().commit();
	                isGroceryDeleted = true; 
			}
			return isGroceryDeleted;
		}
		catch(HibernateException e)
		{
			throw new RecipesPlatformException("Failed deleting a grocery", e);
		}
		finally 
		{
			if (session != null) 
			{
				session.close();
			}
		}
	}
}
