package il.hit.natali.amit.model;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class HibernateCategoryDAOTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddCategory() 
	{
		try {
			if (HibernateCategoryDAO.getInstance().getCategory(100) == null)
			{
				Category newCategory = new Category("sweets");
				HibernateCategoryDAO.getInstance().addCategory(newCategory);
				List <Category> list = HibernateCategoryDAO.getInstance().getAllCategories();
				for(Category category: list)
				{
					if (category.getName().equals(newCategory.getName()))
					{
						assertTrue(HibernateCategoryDAO.getInstance().getCategory(newCategory.getID()) != null);
						return;
					}
				}
			}
		} catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetCategory() {
		try {
			if (HibernateCategoryDAO.getInstance().getCategory(2) != null)
			{
				assertTrue(HibernateCategoryDAO.getInstance().getCategory(2) != null);
				testDeleteCategory();
			}
		} catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testDeleteCategory() {
		try {
			HibernateCategoryDAO.getInstance().deleteCategory(2);
			assertTrue(HibernateCategoryDAO.getInstance().getCategory(2) == null);
		} 
		catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
