package il.hit.natali.amit.model;
import java.util.List;

/**
 * *
 * @author Amit and Natali
 * 
 * This interface allow to manage an object of Category type
 */
public interface ICategoryDAO 
{
	/**
	 * Adds a new Category to the database
	 * 
	 * @param ob The new Category to add to the database
	 * @return The result, true if it succeed and false if it fail
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public boolean addCategory(Category ob) throws RecipesPlatformException;
	/**
	 * Gets a Category from the database based on the id of that category 
	 * 
	 * @param id The id of the Category we want
	 * @return the desired Category object 
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public Category getCategory(int id) throws RecipesPlatformException;
	/**
	 * Gets all the Categories from the database
	 * 
	 * @return a list of all the categories 
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public List<Category> getAllCategories() throws RecipesPlatformException;
	/**
	 * Deletes a Category from the database
	 * 
	 * @param id The id of the Category we want to delete
	 * @return The result, true if it succeed and false if it fail
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public boolean deleteCategory(int id) throws RecipesPlatformException;
}
