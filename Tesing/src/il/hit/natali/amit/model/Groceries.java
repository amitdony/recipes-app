package il.hit.natali.amit.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * This class describes a single grocery
 * 
 * @author Amit and Natali
 * 
 * @param id Id's of this Grocery
 * @param name The name of this Grocery
 * @param Category Describes the Category to which this Grocery belong
 * @param groceries Describes a set of Recipes which this grocery appear in them 
 */
@Entity
@Table(name="GROCERIES")
public class Groceries {
	@Id
    @GeneratedValue
    @Column(name="groceries_id")
	private int id;
	
	private String name;
	
	@ManyToOne
    @JoinColumn(name="category_id")
	private Category category;
	
	@ManyToMany( fetch = FetchType.EAGER, mappedBy="groceries", cascade=CascadeType.REMOVE)
	private Set<Recipes> groceries = new HashSet<Recipes>();
	
	/**
	 * The empty Groceries constructor for supporting java bean
	 */
	public Groceries() {
		super();
	}
	
	/**
	* The Groceries constructor for initializing the Grocery class
	 * 
	 * @param name The Grocery name
	 * @param category The Category of this Grocery
	 */
	public Groceries(String name, Category category) {
		super();
		this.setName(name);
		this.setCategory(category);
	}
	
	/**
	 * Gets the Grocery id
	 * 
	 * @return the Grocery id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Sets the Grocery id
	 * 
	 * @param id The id of the Grocery
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Gets the Grocery name
	 * 
	 * @return the Grocery name 
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the Grocery name
	 * 
	 * @param name The name of the Grocery
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the Category to which this Grocery belong
	 * 
	 * @return The Category of this Grocery
	 */
	public Category getCategory() {
		return category;
	}
	
	/**
	 * Sets the Category to which this Grocery belong
	 * 
	 * @param category The Category of this Grocery
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
	
	/**
	 * Gets a set of Recipes which this Grocery appear in them
	 * 
	 * @return A set of Recipes
	 */
	public Set<Recipes> getRecipes() {
		return groceries;
	}
	
	/**
	 * Sets the set of Recipes which this Grocery appear in them
	 * 
	 * @param recipes A set of Recipes 
	 */
	public void setRecipes(Set<Recipes> recipes) {
		this.groceries = recipes;
	}
	
	/**
	 * Override the toString method which describes the Grocery class 
	 */
	@Override
	public String toString() {
		return "Groceries [id=" + id + ", name=" + name + ", category="
				+ category + ", groceries=" + groceries + "]";
	}
	
	/**
	 * Override the equals method which compare two Groceries id's objects and make sure they are equal   
	 */
	@Override
	public boolean equals(Object obj) {
		Groceries comperTo =(Groceries)obj;
		if(comperTo == null)
			return false;
		else
			return comperTo.id == this.id;
	}
	
	/**
	 * Implements the getHash method based on the id field 
	 */
	@Override
	public int hashCode() {
		return id;
	}
}