package il.hit.natali.amit.model;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class HibernateRecipesDAOTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddRecipe() {
		try {
			if (HibernateRecipesDAO.getInstance().getRecipe(100) == null) {
				Recipes newRecipe = new Recipes("pumpkin cookies",
						"take a pumpkin and put it in the oven", "easy", 1);
				HibernateRecipesDAO.getInstance().addRecipe(newRecipe);
				List<Recipes> list = HibernateRecipesDAO.getInstance()
						.getAllRecipes();
				for (Recipes recipe : list) {
					if (recipe.getName().equals(newRecipe.getName())) {
						assertTrue(HibernateRecipesDAO.getInstance().getRecipe(
								newRecipe.getId()) != null);
						return;
					}
				}
			}
		} catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetRecipe() {
		try {
			if (HibernateRecipesDAO.getInstance().getRecipe(2) != null) {
				assertTrue(HibernateRecipesDAO.getInstance().getRecipe(2) != null);
				testDeleteRecipe();
			}
		} catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testDeleteRecipe() {
		try {
			HibernateRecipesDAO.getInstance().deleteRecipe(2);
			assertTrue(HibernateRecipesDAO.getInstance().getRecipe(2) == null);
		} catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
