package il.hit.natali.amit.model;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 * This class implements the ICategoryDAO interface and using Hibernate access object platform 
 * 
 * @author Amit and Natali
 * 
 * @param factory A sessionFactory field
 * @param instance A singletone field 
 */
public class HibernateCategoryDAO implements ICategoryDAO 
{
	private SessionFactory factory;

	public static HibernateCategoryDAO instance;
	
	/**
	 * Gets the singletone instance
	 * 
	 * @return An HibernateCategoryDAO instance 
	 */
	public static HibernateCategoryDAO getInstance()
	{
		if (instance == null)
		{
			instance = new HibernateCategoryDAO();
		}
		return instance;
	}
	
	/**
	 * private HibernateCategoryDAO constructor
	 */
	private HibernateCategoryDAO()
	{
		factory = new AnnotationConfiguration().configure()
				.buildSessionFactory();
	}

	/**
	 * Adds a new Category to the database
	 */
	@Override
	public boolean addCategory(Category ob) throws RecipesPlatformException 
	{
		Session session = null;
		try
		{
			session = factory.openSession();
			session.beginTransaction();
			session.save(ob);
			session.getTransaction().commit();
			return true;
		}
		catch (HibernateException e)
		{
			if (session != null)
			{
				session.getTransaction().rollback();
			}
			throw new RecipesPlatformException("Failed adding a new category",
					e);
		}
		finally
		{
			if (session != null)
			{
				session.close();
			}
		}

	}
	
	/**
	 * Gets a Category from the database based on the id of that category 
	 */
	@Override
	public Category getCategory(int id) throws RecipesPlatformException {

		Session session = null;
		
		try
		{
			session = factory.openSession();
			return  (Category) session.get(Category.class, id);
			 
		}
		catch(HibernateException e)
		{

			throw new RecipesPlatformException("Failed getting a category", e);			
		}
		finally
		{
			if(session != null)
			{
			session.close();
			}
		}
	
	}

	/**
	 * Gets all the Categories from the database
	 */
	@Override
	public List<Category> getAllCategories() throws RecipesPlatformException {
		Session session = null;
		
		try
		{
			session = factory.openSession();
			return session.createCriteria(Category.class).list();
			 
		}
		catch(HibernateException e)
		{

			throw new RecipesPlatformException("Failed getting all categories", e);			
		}
		finally
		{
			if(session != null)
			{
			session.close();
			}
		}

	}
	
	/**
	 * Deletes a Category from the database
	 */
	@Override
	public boolean deleteCategory(int id) throws RecipesPlatformException 
	{
		Session session = null;
		boolean isCategoryDeleted = false;
		try
		{
			session = factory.openSession();
			Category category;
			 session.beginTransaction();
			if ((category = (Category) session.get(Category.class, id)) != null) 
			{
	                session.delete(category);  
	                session.getTransaction().commit();
	                isCategoryDeleted = true; 
			}
			return isCategoryDeleted;
		}
		catch(HibernateException e)
		{
			throw new RecipesPlatformException("Failed deleting a recipe", e);
		}
		finally 
		{
			if (session != null) 
			{
				session.close();
			}
		}
	}
}
