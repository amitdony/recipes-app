package il.hit.natali.amit.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * This class describes the category of various groceries
 * 
 * @author Amit and Natali
 * 
 * @param id Id's of this Category
 * @param name The name of this Category
 * @param groceries Describe's a list of groceries which belong to this Category
 * @param serialVersionUID Describes the version of the object for backward compatibility
 */
@Entity
@Table(name="CATEGORY")
public class Category implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue
    @Column(name="category_id")
	private int id;
	private String name;
	@OneToMany(mappedBy="category" ,cascade=CascadeType.REMOVE)
	private List<Groceries> groceries;

	/**
	 * The empty Category constructor for supporting java bean
	 */
	public Category() {
		super();
	}

	/**
	 * The Category constructor for initializing the Category class
	 * 
	 * @param name The Category name
	 */
	public Category(String name) {
		super();
		this.setName(name);
	}
	
	/**
	 * Override the toString method which describes the Category class 
	 */
	@Override
	public String toString() {
		return "Category [ID=" + id + ", name=" + name + "]";
	}
	
	/**
	 * Gets the Category id
	 * 
	 * @return the Category id
	 */
	public int getID() {
		return id;
	}
	
	/**
	 * Sets the Category id
	 * 
	 * @param iD The id of the Category
	 */
	public void setID(int iD) {
		id = iD;
	}
	
	/**
	 * Gets the Category name
	 * 
	 * @return the Category name 
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the Category name
	 * 
	 * @param name The name of the Category
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets a list of all the groceries which belongs to this Category
	 * 
	 * @return list of groceries 
	 */
	public List<Groceries> getGroceries() {
		return groceries;
	}
	
	/**
	 * Sets the groceries list of this Category
	 * 
	 * @param groceries The groceries list 
	 */
	public void setGroceries(List<Groceries> groceries) {
		this.groceries = groceries;
	}
}
