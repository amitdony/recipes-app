package il.hit.natali.amit.model;

import java.util.List;

/**
 * 
 * @author Amit and Natali
 *
 * This interface allow to manage an object of Grocery type
 */
public interface IGroceriesDAO 
{
	/**
	 * Adds a new Grocery to the database
	 * 
	 * @param ob The new Grocery to add to the database
	 * @return The result, true if it succeed and false if it fail
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public boolean addGrocery(Groceries ob) throws RecipesPlatformException;
	/**
	 * Gets a Grocery from the database based on the id of that grocery 
	 * 
	 * @param id The id of the Grocery we want
	 * @return the desired Grocery object 
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public Groceries getGrocery(int id) throws RecipesPlatformException;
	/**
	 * Gets all the Groceries from the database
	 * 
	 * @return a list of all the groceries 
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public List<Groceries> getAllGroceries() throws RecipesPlatformException;
	/**
	 * Deletes a Grocery from the database
	 * 
	 * @param id The id of the Grocery we want to delete
	 * @return The result, true if it succeed and false if it fail
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public boolean deleteGrocery(int id) throws RecipesPlatformException;
}
