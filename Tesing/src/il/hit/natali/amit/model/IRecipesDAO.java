package il.hit.natali.amit.model;

import java.util.List;

import il.hit.natali.amit.model.RecipesPlatformException;

/**
 * 
 * @author Amit and Natali
 * 
 * This interface allow to manage an object of Recipe type
 */
public interface IRecipesDAO 
{
	/**
	 * Adds a new Recipe to the database
	 * 
	 * @param ob The new Recipe to add to the database
	 * @return The result, true if it succeed and false if it fail
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public boolean addRecipe(Recipes ob) throws RecipesPlatformException;
	/**
	 * Gets a Recipe from the database based on the id of that recipe 
	 * 
	 * @param id The id of the Recipe we want
	 * @return the desired Recipe object 
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public Recipes getRecipe(int id) throws RecipesPlatformException;
	/**
	 * Deletes a Recipe from the database
	 * 
	 * @param id The id of the Recipe we want to delete
	 * @return The result, true if it succeed and false if it fail
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public boolean deleteRecipe(int id) throws RecipesPlatformException;
	/**
	 * Gets all the Recipes from the database
	 * 
	 * @return a list of all the recipes 
	 * @throws RecipesPlatformException if there is no connection with the server
	 */
	public List<Recipes> getAllRecipes() throws RecipesPlatformException;
}
