package il.hit.natali.amit.model;

/**
 * This class is a wrapper class for an hibernate exception 
 * 
 * @author Amit and Natali
 *
 */
public class RecipesPlatformException extends Exception {
	
	public RecipesPlatformException(String msg, Throwable throwable)
	{
		super(msg, throwable);
	}
}
