package il.hit.natali.amit.model;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class HibernateGroceriesDAOTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddGrocery() {
		try {
				
				Groceries newGrocery = new Groceries();
				newGrocery.setName("Test");
				newGrocery.setCategory(null);
				HibernateGroceriesDAO.getInstance().addGrocery(newGrocery);
				List<Groceries> list = HibernateGroceriesDAO.getInstance()
						.getAllGroceries();
				for (Groceries groceries : list) {
					if (groceries.getName().equals(newGrocery.getName())) {
						assertTrue(HibernateGroceriesDAO.getInstance()
								.getGrocery(newGrocery.getId()) != null);
						return;
					}
				
			}
		} catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetGrocery() {
		try {
			if (HibernateGroceriesDAO.getInstance().getGrocery(2) != null) {
				assertTrue(HibernateGroceriesDAO.getInstance().getGrocery(2) != null);
				testDeleteGrocery();
			}
		} catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testDeleteGrocery() {
		try {
			HibernateGroceriesDAO.getInstance().deleteGrocery(2);
			assertTrue(HibernateGroceriesDAO.getInstance().getGrocery(2) == null);
		} catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
