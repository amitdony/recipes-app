package il.hit.natali.amit.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * This class describes a single Recipe
 * 
 * @author Amit and Natali
 * 
 * @param id Id's of this Recipe
 * @param name The name of this Recipe
 * @param preparation Describes the instruction of this recipe 
 * @param difficulty Describes the difficulty of this recipe
 * @param preparationTime Describes the time it would take to perform this recipe
 * @param groceries Describes the groceries this recipe have
 */
@Entity
@Table(name="RECIPES")
public class Recipes {
	@Id
    @GeneratedValue
    @Column(name="recipes_id")
	private int id;
	
	private String name;
	
	private String preparation;
	
	private String difficulty; 
	
	private int preparationTime; 
	
	 @ManyToMany( fetch = FetchType.EAGER)
	 @JoinTable(name="RECIPES_GROCERIES", 
	 joinColumns={@JoinColumn(name="recipes_id")}, 
	 inverseJoinColumns={@JoinColumn(name="groceries_id")})
	 private Set<Groceries> groceries = new HashSet<Groceries>();
	 
	 /**
	  * The empty Recipes constructor for supporting java bean
	  */
	 public Recipes() {
			super();
		}
	 
	/**
	 * The Recipes constructor for initializing the Recipe class
	 *  
	 * @param name The Recipe name
	 * @param preparation The instruction of this recipe
	 * @param difficulty The difficulty of this recipe
	 * @param preparationTime The time it would take to perform this recipe
	 */
	public Recipes(String name, String preparation, String difficulty,
			int preparationTime) {
		super();
		this.setName(name); 
		this.setPreparation(preparation);
		this.setDifficulty(difficulty);
		this.setPreparationTime(preparationTime);
	}

	/**
	 * Gets the Recipe id
	 * 
	 * @return the Recipe id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the Recipe id
	 * 
	 * @param id The id of the Recipe
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the Recipe name
	 * 
	 * @return the Recipe name 
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the Recipe name
	 * 
	 * @param name The name of the Recipe
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the instruction of this recipe
	 * 
	 * @return Instruction of this recipe
	 */
	public String getPreparation() {
		return preparation;
	}
	
	/**
	 * Sets the instruction of this recipe
	 * 
	 * @param preparation Instruction of a recipe
	 */
	public void setPreparation(String preparation) {
		this.preparation = preparation;
	}
	
	/**
	 * Gets the difficulty of this recipe
	 * 
	 * @return Difficulty of this recipe
	 */
	public String getDifficulty() {
		return difficulty;
	}
	
	/**
	 * Sets the difficulty of this recipe
	 * 
	 * @param difficulty Difficulty of a recipe
	 */
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}
	
	/**
	 * Gets the time it would take to perform this recipe
	 * 
	 * @return A number of minutes to perform this recipe 
	 */
	public int getPreparationTime() {
		return preparationTime;
	}

	/**
	 *  Sets the time it would take to perform this recipe
	 * 
	 * @param preparationTime Number of minutes to perform this recipe
	 */
	public void setPreparationTime(int preparationTime) {
		if(preparationTime>=0)
			this.preparationTime = preparationTime;
		else
			this.preparationTime=0;
	}

	/**
	 * Gets a set of Groceries which this Recipe have
	 * 
	 * @return A set of Groceries
	 */
	public Set<Groceries> getGroceries() {
		return groceries;
	}

	/**
	 * Sets the set of Groceries which this Recipe have
	 * 
	 * @param groceries A set of Groceries
	 */
	public void setGroceries(Set<Groceries> groceries) {
		this.groceries = groceries;
	}

	/**
	 * Override the toString method which describes the Recipe class 
	 */
	@Override
	public String toString() {
		return "Recipes [id=" + id + ", name=" + name + ", preparation="
				+ preparation + ", difficulty=" + difficulty
				+ ", preparationTime=" + preparationTime + "]";
	}
	
	/**
	 * Override the equals method which compare two Recipes id's objects and make sure they are equal   
	 */
	@Override
	public boolean equals(Object obj) {
		Recipes comperTo =(Recipes)obj;
		if(comperTo == null)
			return false;
		else
			return comperTo.id == this.id;
	}
	
	/**
	 * Implements the getHash method based on the id field 
	 */
	@Override
	public int hashCode() {
	
		return id;
	}
}
