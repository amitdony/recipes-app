package il.hit.natali.amit;

import il.hit.natali.amit.model.Category;
import il.hit.natali.amit.model.Groceries;
import il.hit.natali.amit.model.HibernateCategoryDAO;
import il.hit.natali.amit.model.HibernateGroceriesDAO;
import il.hit.natali.amit.model.HibernateRecipesDAO;
import il.hit.natali.amit.model.ICategoryDAO;
import il.hit.natali.amit.model.IGroceriesDAO;
import il.hit.natali.amit.model.IRecipesDAO;
import il.hit.natali.amit.model.Recipes;
import il.hit.natali.amit.model.RecipesPlatformException;

import java.util.Set;

public class Program {

	public static void main(String[] args) {

		ICategoryDAO daoCategory = HibernateCategoryDAO.getInstance();
		IGroceriesDAO daoGroceries= HibernateGroceriesDAO.getInstance();
		IRecipesDAO daoRecipes = HibernateRecipesDAO.getInstance();
		
		try 
		{			
			daoCategory.addCategory(new Category("hazila"));
		} catch (RecipesPlatformException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
