package il.hit.natali.amit.controller;

import il.hit.natali.amit.model.Category;
import il.hit.natali.amit.model.Groceries;
import il.hit.natali.amit.model.HibernateCategoryDAO;
import il.hit.natali.amit.model.HibernateGroceriesDAO;
import il.hit.natali.amit.model.HibernateRecipesDAO;
import il.hit.natali.amit.model.ICategoryDAO;
import il.hit.natali.amit.model.IGroceriesDAO;
import il.hit.natali.amit.model.IRecipesDAO;
import il.hit.natali.amit.model.Recipes;
import il.hit.natali.amit.model.RecipesPlatformException;
import java.io.Console;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;
import org.apache.log4j.*;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Servlet implementation class Controller
 * 
 * @author Amit and Natali
 * 
 * @param serialVersionUID
 *            Describes the version of the object for backward compatibility
 * @param daoCategory
 *            An hibernate Category DAO instance
 * @param daoGroceries
 *            An hibernate Groceries DAO instance
 * @param daoRecipes
 *            An hibernate Recipes DAO instance
 * @param logger
 *            Allow to represent log messages(log4j)
 */
@WebServlet("/Controller/*")
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ICategoryDAO daoCategory;
	private IGroceriesDAO daoGroceries;
	private IRecipesDAO daoRecipes;
	static Logger logger;

	/**
	 * The Controller constructor for initializing the Controller class
	 */
	public Controller() {
		super();
		BasicConfigurator.configure();
		LogManager.getRootLogger().setLevel(Level.INFO);
		daoCategory = HibernateCategoryDAO.getInstance();
		daoGroceries = HibernateGroceriesDAO.getInstance();
		daoRecipes = HibernateRecipesDAO.getInstance();
		logger = Logger.getLogger("Controller");
	}

	/**
	 * Manages Post request
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	};

	/**
	 * Manages Get request
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String path = request.getPathInfo();
		path = path.substring(1);
		try {
			switch (path) {
			case "about": {
				logger.info("The user pressed the about button");
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/aboutscreen.jsp");
				dispatcher.forward(request, response);
				break;
			}
			case "login": {
				handleLoginRequest(request, response);
				break;
			}
			case "category": {
				handleCategoryRequest(request, response);
				break;
			}
			case "groceries": {
				handleGroceriesRequest(request, response);
				break;
			}
			case "recipes": {
				handleRecipesRequest(request, response);
				break;
			}
			case "search": {
				handleSearchRequest(request, response);
				break;
			}

			case "addRecipe": {
				handleAddRecipeRequest(request, response);
				break;
			}

			case "addGrocerie": {
				handleAddGrocerieRequest(request, response);
				break;
			}

			case "addCategory": {
				handleAddCategoryRequest(request, response);
				break;
			}
			case "editCategories": {
				handleEditCategoriesRequest(request, response);
				break;
			}
			case "editGroceries": {
				handleEditGroceriesRequest(request, response);
				break;
			}
			case "editRecipes": {
				handleEditRecipesRequest(request, response);
				break;
			
			}
			
			case "index":{
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/Index.jsp");
				dispatcher.forward(request, response);
				break;
			}

			default: {
				request.setAttribute("Details","page not found");
				RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ErrorPage.jsp");
				dispatcher.forward(request, response);
				break;
			}
			}

		}
		catch (RecipesPlatformException e) {
			logger.error("An error occurred"+e.getMessage());
			e.printStackTrace();
			request.setAttribute("Details","RecipesPlatformException - "+ e.getMessage());
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ErrorPage.jsp");
			dispatcher.forward(request, response);
		}
		catch (Exception e) {
			logger.error("An error occurred"+e.getMessage());
			e.printStackTrace();
			request.setAttribute("Details","Exception - "+ e.getMessage());
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/ErrorPage.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * Manage all the request to delete a recipe
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleEditRecipesRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		logger.info("The admin pressed the edit recipes button");
		String recipeID = request.getParameter("id");
		String method = request.getParameter("method");

		if (recipeID != null && method != null) {
			int id = Integer.parseInt(recipeID);
			daoRecipes.deleteRecipe(id);
		}
		request.setAttribute("recipes", daoRecipes.getAllRecipes());
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/editRecipes.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * Manage all the request to delete a grocery
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleEditGroceriesRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		logger.info("The admin pressed the edit Groceries button");
		String recipeID = request.getParameter("id");
		String method = request.getParameter("method");

		if (recipeID != null && method != null) {
			int id = Integer.parseInt(recipeID);
			daoGroceries.deleteGrocery(id);
		}
		request.setAttribute("groceries", daoGroceries.getAllGroceries());
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/editGroceries.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * Manage all the request to delete a category
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleEditCategoriesRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		logger.info("The admin pressed the edit Categories button");
		String recipeID = request.getParameter("id");
		String method = request.getParameter("method");

		if (recipeID != null && method != null) {
			int id = Integer.parseInt(recipeID);
			daoCategory.deleteCategory(id);
		}
		request.setAttribute("categories", daoCategory.getAllCategories());
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher("/editCategories.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * Manage all the request to add a category
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleAddCategoryRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		String name = request.getParameter("name");

		if (name != null) {
			Category toAdd = new Category();
			toAdd.setName(name);
			try {
				daoCategory.addCategory(toAdd);
				request.setAttribute("categories",
						daoCategory.getAllCategories());
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/editCategories.jsp");
				dispatcher.forward(request, response);
			} catch (RecipesPlatformException ex) {
				logger.error("Cant add new Recipe" + ex.getMessage());
				// show error form...
			}
		} else {
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/addCategory.jsp");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * Manage all the request to add a grocery
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleAddGrocerieRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		String name = request.getParameter("name");
		String categoryID = request.getParameter("Category");

		if (name != null && categoryID != null) {
			Groceries toAdd = new Groceries();
			toAdd.setName(name);
			toAdd.setCategory(daoCategory.getCategory(Integer
					.parseInt(categoryID)));
			try {
				daoGroceries.addGrocery(toAdd);
				request.setAttribute("groceries",
						daoGroceries.getAllGroceries());
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/editGroceries.jsp");
				dispatcher.forward(request, response);
			} catch (RecipesPlatformException ex) {
				logger.error("Cant add new Recipe" + ex.getMessage());
				// show error form...
			}
		} else {
			request.setAttribute("Categories", daoCategory.getAllCategories());
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/addGrocerie.jsp");
			dispatcher.forward(request, response);
		}
	}
	
	/**
	 * Manage all the request to add a recipe
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleAddRecipeRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		String name = request.getParameter("name");
		String preparationTime = request.getParameter("time");
		String difficulty = request.getParameter("Difficulty");
		String preparation = request.getParameter("Preparation");
		String[] groceries = request.getParameterValues("Groceries");

		if (name != null && preparationTime != null && difficulty != null
				&& preparation != null && groceries != null) {
			Recipes toAdd = new Recipes(name, preparation, difficulty,
					Integer.parseInt(preparationTime));
			HashSet<Groceries> groceriesSet = new HashSet<>();
			for (String id : groceries) {
				groceriesSet.add(daoGroceries.getGrocery(Integer.parseInt(id)));
			}
			toAdd.setGroceries(groceriesSet);
			try {
				daoRecipes.addRecipe(toAdd);
				request.setAttribute("recipes", daoRecipes.getAllRecipes());
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/editRecipes.jsp");
				dispatcher.forward(request, response);
			} catch (RecipesPlatformException ex) {
				logger.error("Cant add new Recipe" + ex.getMessage());
				// show error form...
			}
		} else {
			request.setAttribute("groceries", daoGroceries.getAllGroceries());
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/addRecipe.jsp");
			dispatcher.forward(request, response);
		}
	}
	
	/**
	 * Manage the operation of searching a recipe by groceries the user have
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleSearchRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		logger.info("The user pressed the search button");
		String[] grocersToSearch = request.getParameterValues("Groceries");

		if (grocersToSearch != null) {
			List<Recipes> recipesResults = new ArrayList<Recipes>();
			recipesResults.addAll(daoGroceries.getGrocery(
					Integer.parseInt(grocersToSearch[0])).getRecipes());

			for (String grocerieID : grocersToSearch) {
				recipesResults.retainAll(daoGroceries.getGrocery(
						Integer.parseInt(grocerieID)).getRecipes());
			}

			request.setAttribute("recipes", recipesResults);
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/recipes.jsp");
			dispatcher.forward(request, response);
			logger.info("a list of recipes based on the user groceries selection is now presented to the user");
		} else {
			logger.info("the user did not selected anything");
			request.setAttribute("groceries", daoGroceries.getAllGroceries());
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/search.jsp");
			dispatcher.forward(request, response);
		}
	}
	
	/**
	 * Introduce a single recipe or a list of recipes
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleRecipesRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		logger.info("The user pressed the recipes button");
		String recipeID = request.getParameter("id");

		if (recipeID != null) {
			int index = Integer.parseInt(recipeID);
			request.setAttribute("recipe", daoRecipes.getRecipe(index));
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/recipe.jsp");
			dispatcher.forward(request, response);
			logger.info("The user is watching the " + recipeID + "recipe");
		} else {
			request.setAttribute("recipes", daoRecipes.getAllRecipes());
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/recipes.jsp");
			dispatcher.forward(request, response);
		}
	}
	
	/**
	 * Introduce a single grocery or a list of groceries
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleGroceriesRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		logger.info("The user pressed the groceries button");
		String grocerieID = request.getParameter("id");

		if (grocerieID != null) {
			int index = Integer.parseInt(grocerieID);
			request.setAttribute("grocery", daoGroceries.getGrocery(index));
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/grocery.jsp");
			dispatcher.forward(request, response);
			logger.info("The user is watching the " + grocerieID + "grocery");
		} else {
			request.setAttribute("groceries", daoGroceries.getAllGroceries());
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/groceries.jsp");
			dispatcher.forward(request, response);
		}
	}
	
	/**
	 * Introduce a single category or a list of categories
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws RecipesPlatformException
	 *             if there is no connection with the server
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 */
	private void handleCategoryRequest(HttpServletRequest request,
			HttpServletResponse response) throws RecipesPlatformException,
			ServletException, IOException {
		logger.info("The user pressed the category button");
		String categoryID = request.getParameter("id");

		if (categoryID != null) {
			int index = Integer.parseInt(categoryID);
			request.setAttribute("category", daoCategory.getCategory(index));
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/category.jsp");
			dispatcher.forward(request, response);
			logger.info("The user is watching the " + categoryID + "category");
		} else {
			request.setAttribute("categories", daoCategory.getAllCategories());
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/categories.jsp");
			dispatcher.forward(request, response);
		}
	}
	
	/**
	 * Manage a login request
	 * 
	 * @param request
	 *            The client request
	 * @param response
	 *            Specify the response which return to the client
	 * @throws ServletException
	 *             Defines a general exception a servlet can throw when it
	 *             encounters difficulty
	 * @throws IOException
	 *             This class is the general class of exceptions produced by
	 *             failed or interrupted I/O operations
	 * @throws NoSuchAlgorithmException Thrown when a particular cryptographic algorithm is requested but is not available in the environment
	 * @throws NoSuchProviderException Thrown when a particular security provider is requested but is not available in the environment
	 */
	private void handleLoginRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException,
			NoSuchAlgorithmException, NoSuchProviderException {
		logger.info("The user pressed the login button");
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		if (username != null && password != null) {
			if (isValidUser(username, password)) {
				logger.info("administrator is entering the application");
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/manager.jsp");
				dispatcher.forward(request, response);
			} else {
				request.setAttribute("context",
						"The password / UserName is not mach");
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher("/login.jsp");
				dispatcher.forward(request, response);
			}
		} else {
			logger.info("there was an attempt to enter the application with invalid credenitals");
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher("/login.jsp");
			dispatcher.forward(request, response);
		}
	}
	
	/**
	 * Checks the correctness of a password and user name fields  
	 * 
	 * @param username The user name 
	 * @param password The password
	 * @return The result, if it is a legal user name and legal password
	 * @throws NoSuchAlgorithmException Thrown when a particular cryptographic algorithm is requested but is not available in the environment
	 * @throws NoSuchProviderException Thrown when a particular security provider is requested but is not available in the environment
	 */
	private boolean isValidUser(String username, String password)
			throws NoSuchAlgorithmException, NoSuchProviderException {
		boolean isValidPassword = false;
		String validUserName = "admin";
		String passwordToHash = "admin";

		String securePassword = getSecurePassword(passwordToHash);
		String passowrdToVerify = getSecurePassword(password);

		if (validUserName.equals(username)
				&& passowrdToVerify.equals(securePassword)) {
			isValidPassword = true;
		}
		return isValidPassword;
	}
	
	/**
	 * Gets a secure password by using MD5 algorithm(hashing the password)
	 * 
	 * @param passwordToHash The password which we want to hash
	 * @return The Hashed password
	 */
	private static String getSecurePassword(String passwordToHash) {
		String generatedPassword = null;
		try {
			// Create MessageDigest instance for MD5
			MessageDigest md = MessageDigest.getInstance("MD5");
			// Get the hash's bytes
			byte[] bytes = md.digest(passwordToHash.getBytes());
			// This bytes[] has bytes in decimal format;
			// Convert it to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			// Get complete hashed password in hex format
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}
}
